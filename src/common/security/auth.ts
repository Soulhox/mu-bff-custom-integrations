import { AxiosRequestConfig } from 'axios';

export enum HeaderKeys {
  ACCESS_TOKEN = 'access_token',
}

export class Auth {
  static getAccessToken(value: string): AxiosRequestConfig {
    return {
      headers: {
        access_token: value,
      },
    };
  }
}
