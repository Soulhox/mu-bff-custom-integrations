import {
  CanActivate,
  ExecutionContext,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { PhalconApiException } from 'src/common/exceptions/api-exception';

import { HttpUtils } from '../../../libs/http';
import { MuLogger } from '../../../libs/logging';
import { Auth } from './auth';

@Injectable()
export class AuthGuard implements CanActivate {
  static instance: AuthGuard;
  private httpClient: AxiosInstance;
  private readonly logger: MuLogger = new MuLogger(
    `${MuLogger.MU_LOGGER_KEY}${AuthGuard.name}`,
  );

  constructor() {
    if (AuthGuard.instance) {
      return AuthGuard.instance;
    }
    this.httpClient = HttpUtils.createAxiosClient({
      baseURL:
        process.env.AUTH_SERVER_URL || 'https://dev.api.mensajerosurbanos.com',
      timeout: 60000,
    });
    AuthGuard.instance = this;
  }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const accessToken: string =
      context.switchToHttp().getRequest().headers?.access_token ?? '';
    if (!accessToken) {
      return this.reject();
    }
    const resp = await this.httpClient
      .get('/user/information', Auth.getAccessToken(accessToken))
      .then(
        (res) => res.data,
        (e) =>
          Promise.reject(new PhalconApiException(e.message, e.response.status)),
      );
    return resp.status === 'success' ? Promise.resolve(true) : this.reject();
  }

  private reject(): Promise<boolean> {
    return Promise.reject(
      new PhalconApiException('Forbidden', HttpStatus.FORBIDDEN),
    );
  }
}
