import { Test, TestingModule } from '@nestjs/testing';
import { CommonConfig, MU_PHALCON_CLIENT_FACTORY } from '../../common.config';
import { CommonService } from '../../core/common.service';
import { OauthApiProvider } from '../../infrastructure/providers/oauth-api.provider';

describe('CommonService', () => {
  let service: CommonService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommonService,
        CommonConfig,
        MU_PHALCON_CLIENT_FACTORY,
        { provide: 'IOauthApi', useClass: OauthApiProvider },
      ],
    }).compile();

    service = module.get<CommonService>(CommonService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
