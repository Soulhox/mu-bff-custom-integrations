import { Test, TestingModule } from '@nestjs/testing';
import { PhalconApiException } from '../../exceptions/api-exception';

describe('PhalconApiException', () => {
  let provider: PhalconApiException;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PhalconApiException],
    }).compile();

    provider = module.get<PhalconApiException>(PhalconApiException);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
