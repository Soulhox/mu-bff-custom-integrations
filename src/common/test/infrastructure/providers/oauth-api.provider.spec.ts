import { HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import {
  CommonConfig,
  MU_PHALCON_CLIENT,
  MU_PHALCON_CLIENT_FACTORY,
} from '../../../common.config';
import { OauthApiProvider } from '../../../infrastructure/providers/oauth-api.provider';

describe('OauthApiProvider', () => {
  let provider: OauthApiProvider;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        OauthApiProvider,
        CommonConfig,
        MU_PHALCON_CLIENT_FACTORY,
        { provide: MU_PHALCON_CLIENT, useValue: mockOauthProvider },
      ],
    }).compile();

    provider = module.get<OauthApiProvider>(OauthApiProvider);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });

  it('should return service unavaiable getAccessTokenInformation', () => {
    mockOauthProvider.get.mockImplementation(() =>
      Promise.reject({
        message: '',
        status: HttpStatus.SERVICE_UNAVAILABLE,
      }),
    );
    provider.getAccessTokenInformation('token').catch((e) => {
      expect(e.status).toBe(HttpStatus.SERVICE_UNAVAILABLE);
    });
  });
});

const mockOauthProvider = {
  get: jest.fn(),
};
