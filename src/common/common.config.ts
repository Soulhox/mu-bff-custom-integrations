import { Injectable } from '@nestjs/common';
import { HttpUtils } from '../../libs/http';

@Injectable()
export class CommonConfig {
  get url(): string {
    return process.env.MU_PHALCON_URL;
  }
}

export const MU_PHALCON_CLIENT = 'MU_PHALCON_CLIENT';

export const MU_PHALCON_CLIENT_FACTORY = {
  provide: MU_PHALCON_CLIENT,
  useFactory: (config: CommonConfig) => {
    return HttpUtils.createAxiosClient({
      baseURL: config.url,
      timeout: 2000000,
    });
  },
  inject: [CommonConfig],
};
