import { AccessTokenResponseDto } from '../../dtos/access-token-response.dto';

export interface IOauthApi {
  getAccessTokenInformation(token: string): Promise<AccessTokenResponseDto>;
}
