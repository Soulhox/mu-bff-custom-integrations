import { Inject, Injectable } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { MU_PHALCON_CLIENT } from '../../common.config';
import { AccessTokenResponseDto } from '../../dtos/access-token-response.dto';
import { PhalconApiException } from '../../exceptions/api-exception';

@Injectable()
export class OauthApiProvider {
  constructor(
    @Inject(MU_PHALCON_CLIENT) private readonly httpClient: AxiosInstance,
  ) {}
  async getAccessTokenInformation(
    token: string,
  ): Promise<AccessTokenResponseDto> {
    const params = {
      params: {
        access_token: token,
      },
    };

    return this.httpClient
      .get<AccessTokenResponseDto>('/oauth/resources', params)
      .then(
        (res) => res.data,
        (e) => Promise.reject(new PhalconApiException(e.message, e.status)),
      );
  }
}
