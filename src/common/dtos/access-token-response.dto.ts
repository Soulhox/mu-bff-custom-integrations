export interface AccessTokenResponseDto {
  id: string;
  username: string;
  email: string;
  user_type: string;
  company: string;
  city: string;
  name_city: string;
  name: string;
  identification: string;
  create_at: string;
  administrator: boolean;
  lastvisit_at: string;
  company_id: string;
  view_services: string;
  number_users: number;
  roles: [];
  status: number;
  status_user: number;
  time_end: number;
  city_phone: string;
  terms: string;
  country: number;
  company_name: string;
  updated_data: boolean;
  roles_id: [];
  num_services: number;
}
