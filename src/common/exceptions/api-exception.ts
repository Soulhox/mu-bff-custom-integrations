import { HttpException } from '@nestjs/common';

export class PhalconApiException extends HttpException {
  constructor(message: string, statusCode: number) {
    super(message, statusCode);
  }
}
