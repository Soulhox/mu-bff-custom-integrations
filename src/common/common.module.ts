import { Module } from '@nestjs/common';
import { CommonConfig, MU_PHALCON_CLIENT_FACTORY } from './common.config';
import { CommonService } from './core/common.service';
import { OauthApiProvider } from './infrastructure/providers/oauth-api.provider';

@Module({
  providers: [
    CommonService,
    { provide: 'IOauthApi', useClass: OauthApiProvider },
    MU_PHALCON_CLIENT_FACTORY,
    CommonConfig,
  ],
  exports: [CommonService],
})
export class CommonModule {}
