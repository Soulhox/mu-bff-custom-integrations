import { Inject, Injectable } from '@nestjs/common';
import { MuLogger } from '../../../libs/logging';
import { AccessTokenResponseDto } from '../dtos/access-token-response.dto';
import { IOauthApi } from '../infrastructure/interfaces/oauth-api.interface';

@Injectable()
export class CommonService {
  private readonly logger: MuLogger = new MuLogger(
    `${MuLogger.MU_LOGGER_KEY}${CommonService.name}`,
  );

  constructor(@Inject('IOauthApi') private readonly oauthApi: IOauthApi) {}

  async getAccessTokenInformation(
    accessToken: string,
  ): Promise<AccessTokenResponseDto> {
    this.logger.log(`[getAccessTokenInformationRequest]: ${accessToken}`);
    const response = await this.oauthApi.getAccessTokenInformation(accessToken);
    this.logger.log(
      `[getAccessTokenInformationResponse]: - ${JSON.stringify(response)}`,
    );
    return response;
  }
}
