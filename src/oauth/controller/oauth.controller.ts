import { Controller, Post } from '@nestjs/common';
import { RequestHeader } from '../../common/validators/validate-header';
import { OauthService } from '../core/oauth.service';
import { OauthTokenRequest } from '../dtos/oauthTokenRequest.dto';

@Controller('oauth')
export class OauthController {
  constructor(private oauthService: OauthService) {}

  @Post('token')
  create(@RequestHeader(OauthTokenRequest) request: OauthTokenRequest) {
    return this.oauthService.createSesionToken(request);
  }
}
