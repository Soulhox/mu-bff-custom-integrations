import { Module } from '@nestjs/common';
import { OauthController } from './controller/oauth.controller';
import { OauthService } from './core/oauth.service';
import { PhalconApiProvider } from './infrastructure/providers/phalcon.api.provider';
import { MuPhalconConfig, MU_PHALCON_CLIENT_FACTORY } from './oauth.config';

@Module({
  controllers: [OauthController],
  providers: [
    OauthService,
    PhalconApiProvider,
    { provide: 'IApiProvider', useClass: PhalconApiProvider },
    MU_PHALCON_CLIENT_FACTORY,
    MuPhalconConfig,
  ],
})
export class OauthModule {}
