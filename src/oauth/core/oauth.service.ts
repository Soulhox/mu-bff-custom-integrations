import { Inject, Injectable } from '@nestjs/common';
import { MuLogger } from '../../../libs/logging';
import { OauthTokenRequest } from '../dtos/oauthTokenRequest.dto';
import { OauthTokenResponse } from '../dtos/oauthTokenResponse.dto';
import { IApiProvider } from '../infrastructure/interfaces/api.provider';

@Injectable()
export class OauthService {
  private readonly logger: MuLogger = new MuLogger(
    `${MuLogger.MU_LOGGER_KEY}${OauthService.name}`,
  );

  constructor(@Inject('IApiProvider') private readonly api: IApiProvider) {}

  async createSesionToken(
    request: OauthTokenRequest,
  ): Promise<OauthTokenResponse> {
    this.logger.log(
      `[CreateTokenRequest]: ${JSON.stringify(request.client_id)}`,
    );
    const response = await this.api.getAuthToken(request);
    this.logger.log(`[CreateTokenResponse]: - ${JSON.stringify(response)}`);
    return response;
  }
}
