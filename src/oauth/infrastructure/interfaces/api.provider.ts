import { OauthTokenRequest } from '../../dtos/oauthTokenRequest.dto';
import { OauthTokenResponse } from '../../dtos/oauthTokenResponse.dto';

export interface IApiProvider {
  getAuthToken(request: OauthTokenRequest): Promise<OauthTokenResponse>;
}
