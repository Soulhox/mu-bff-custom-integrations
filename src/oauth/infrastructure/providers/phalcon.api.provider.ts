import { OauthTokenResponse } from '../../dtos/oauthTokenResponse.dto';
import { Inject } from '@nestjs/common';
import { AxiosInstance } from 'axios';
import { MU_PHALCON_CLIENT } from '../../oauth.config';
import { IApiProvider } from '../interfaces/api.provider';
import { OauthTokenRequest } from '../../../oauth/dtos/oauthTokenRequest.dto';
import { PhalconApiException } from '../../../common/exceptions/api-exception';

export class PhalconApiProvider implements IApiProvider {
  constructor(
    @Inject(MU_PHALCON_CLIENT) private readonly httpClient: AxiosInstance,
  ) {}

  async getAuthToken(request: OauthTokenRequest): Promise<OauthTokenResponse> {
    return this.httpClient
      .post<OauthTokenResponse>('/oauth/token', request)
      .then(
        (res) => res.data,
        (e) => Promise.reject(new PhalconApiException(e.message, e.status)),
      );
  }
}
