import { HttpStatus } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { OauthTokenResponse } from '../../../oauth/dtos/oauthTokenResponse.dto';
import { OauthTokenRequest } from '../../../oauth/dtos/oauthTokenRequest.dto';
import { PhalconApiProvider } from '../../infrastructure/providers/phalcon.api.provider';
import { MU_PHALCON_CLIENT } from '../../oauth.config';

describe('PhalconApiProvider', () => {
  let phalconApiProvider: PhalconApiProvider;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        PhalconApiProvider,
        { provide: MU_PHALCON_CLIENT, useValue: mockMuPhlaconClient },
      ],
    }).compile();
    phalconApiProvider = module.get<PhalconApiProvider>(PhalconApiProvider);
  });
  it('should be defined', () => {
    expect(phalconApiProvider).toBeDefined();
  });
  it('should return success turbo validation', async () => {
    const oauthTokenResponse = new OauthTokenResponse();
    mockMuPhlaconClient.post.mockImplementation(() =>
      Promise.resolve({
        status: HttpStatus.OK,
        data: oauthTokenResponse,
      }),
    );
    const response = await phalconApiProvider.getAuthToken(
      new OauthTokenRequest(),
    );
    expect(response).toBe(oauthTokenResponse);
  });
  it('should return service unavaiable response status', () => {
    mockMuPhlaconClient.post.mockImplementation(() =>
      Promise.reject({
        message: '',
        status: HttpStatus.SERVICE_UNAVAILABLE,
      }),
    );
    phalconApiProvider.getAuthToken(new OauthTokenRequest()).catch((e) => {
      expect(e.status).toBe(HttpStatus.SERVICE_UNAVAILABLE);
    });
  });
});

const mockMuPhlaconClient = {
  post: jest.fn(),
};
