import { Test } from '@nestjs/testing';
import { PhalconApiProvider } from '../../infrastructure/providers/phalcon.api.provider';
import { OauthService } from '../../core/oauth.service';
import { MU_PHALCON_CLIENT } from '../../../oauth/oauth.config';

describe('OauthService', () => {
  let oauthService: OauthService;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        OauthService,
        { provide: PhalconApiProvider, useValue: mockApiProvider },
        { provide: 'IApiProvider', useClass: PhalconApiProvider },
        { provide: MU_PHALCON_CLIENT, useValue: mockMuPhlaconClient },
      ],
    }).compile();
    oauthService = module.get<OauthService>(OauthService);
  });
  it('should be defined', () => {
    expect(oauthService).toBeDefined();
  });
});

const mockApiProvider = {};

const mockMuPhlaconClient = {};
