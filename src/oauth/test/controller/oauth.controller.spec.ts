import { Test } from '@nestjs/testing';
import { OauthService } from '../../core/oauth.service';
import { MuLogger } from '../../../../libs/logging';
import { OauthController } from '../../controller/oauth.controller';
import { OauthTokenRequest } from '../../../oauth/dtos/oauthTokenRequest.dto';

describe('OauthController', () => {
  let oauthController: OauthController;
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        OauthController,
        { provide: MuLogger, useValue: mockMuLogger },
        { provide: OauthService, useValue: mockOauthService },
      ],
    }).compile();
    oauthController = module.get<OauthController>(OauthController);
  });
  it('should be defined', () => {
    expect(oauthController).toBeDefined();
  });
  it('should be defined', () => {
    mockOauthService.createSesionToken.mockReturnValue({});
    oauthController.create(new OauthTokenRequest());
  });
});

const mockMuLogger = {};

const mockOauthService = {
  createSesionToken: jest.fn(),
};
