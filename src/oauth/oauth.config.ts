import { Injectable } from '@nestjs/common';
import { HttpUtils } from '../../libs/http';

@Injectable()
export class MuPhalconConfig {
  get url(): string {
    return process.env.AUTH_SERVER_URL;
  }
}

export const MU_PHALCON_CLIENT = 'MU_PHALCON_CLIENT';
export const MU_PHALCON_CLIENT_FACTORY = {
  provide: MU_PHALCON_CLIENT,
  useFactory: (config: MuPhalconConfig) => {
    return HttpUtils.createAxiosClient({
      baseURL: config.url,
      timeout: 2000000,
    });
  },
  inject: [MuPhalconConfig],
};
