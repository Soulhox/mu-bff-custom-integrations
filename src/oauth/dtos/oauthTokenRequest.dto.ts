import { Expose } from 'class-transformer';
import { IsDefined, IsNotEmpty } from 'class-validator';
import { MessageCodes } from '../../common/dtos/enums/message-codes';

export class OauthTokenRequest {
  readonly grant_type: string = 'client_credentials';

  @IsDefined()
  @Expose({ name: 'client_id' })
  @IsNotEmpty({ message: MessageCodes.CLIENT_ID_REQUIRED })
  readonly client_id: string;

  @IsDefined()
  @Expose({ name: 'client_secret' })
  @IsNotEmpty({ message: MessageCodes.CLIENT_SECRET_REQUIRED })
  readonly client_secret: string;
}
