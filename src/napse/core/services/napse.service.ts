import { Inject, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { MuLogger } from 'libs/logging';
import { CustomIntegrationApi } from '../../infrastructure/interfaces/custom-integration-interface';
import {
  CreateOrderRequest,
  CreateOrderResponse,
  OrderStatusResponse,
} from '../../../napse/dtos/dto-napse';

@Injectable()
export class NapseService {
  private readonly logger: MuLogger = new MuLogger(
    `${MuLogger.MU_LOGGER_KEY}${NapseService.name}`,
  );
  constructor(
    @Inject('CustomIntegrationApi') private readonly api: CustomIntegrationApi,
  ) {}
  createOrder(
    request: CreateOrderRequest,
    config: any,
  ): Promise<CreateOrderResponse> {
    this.logger.debug(
      `[createOrderRequest]: {request: ${JSON.stringify(request)}}`,
    );
    return this.api.createOrder(request, config);
  }
  getOrderStatus(orderId: any, config: any): Promise<OrderStatusResponse> {
    this.logger.debug(
      `[getOrderStatusRequest]: {order_id: ${JSON.stringify(orderId)}}`,
    );
    return this.api.getOrderStatus(orderId, config);
  }
}
