import {
  CreateOrderRequest,
  CreateOrderResponse,
  OrderStatusResponse,
} from '../../dtos/dto-napse';

export interface CustomIntegrationApi {
  createOrder(
    request: CreateOrderRequest,
    config: any,
  ): Promise<CreateOrderResponse>;
  getOrderStatus(orderId: any, config: any): Promise<OrderStatusResponse>;
}
