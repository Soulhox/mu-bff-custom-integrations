import {
  CreateOrderRequest,
  CreateOrderResponse,
  OrderStatusResponse,
} from 'src/napse/dtos/dto-napse';
import { CustomIntegrationApi } from '../interfaces/custom-integration-interface';

export class CustomIntegrationProvider implements CustomIntegrationApi {
  async createOrder(
    request: CreateOrderRequest,
    config: any,
  ): Promise<CreateOrderResponse> {
    return {
      message: 'message',
      status: 200,
    };
  }
  async getOrderStatus(
    orderId: any,
    config: any,
  ): Promise<OrderStatusResponse> {
    return {
      order_id: orderId,
      status: 'OK',
    };
  }
}
