import {
  Controller,
  UsePipes,
  ValidationPipe,
  Post,
  Get,
  UseGuards,
  Body,
  Res,
  Headers,
  Param,
} from '@nestjs/common';
import { MuLogger } from 'libs/logging';
import { HeaderKeys } from '../../common/security/auth';
import { AuthGuard } from '../../common/security/auth-guard';
import { NapseService } from '../core/services/napse.service';
import {
  CreateOrderRequest,
  CreateOrderResponse,
  OrderStatusResponse,
} from '../dtos/dto-napse';

@Controller('napse')
@UsePipes(new ValidationPipe())
export class NapseController {
  private readonly logger: MuLogger = new MuLogger(
    `${MuLogger.MU_LOGGER_KEY}${NapseController.name}`,
  );
  constructor(private napseService: NapseService) {}
  @Post('order')
  @UseGuards(AuthGuard)
  createOrder(
    @Headers(HeaderKeys.ACCESS_TOKEN) accessToken: string,
    @Body() request: CreateOrderRequest,
  ): Promise<CreateOrderResponse> {
    return this.napseService.createOrder(request, accessToken);
  }

  @Get('order/:order_id')
  @UseGuards(AuthGuard)
  getOrderStatus(
    @Headers(HeaderKeys.ACCESS_TOKEN) accessToken: string,
    @Param('order_id') orderId,
  ): Promise<OrderStatusResponse> {
    return this.napseService.getOrderStatus(orderId, accessToken);
  }
}
