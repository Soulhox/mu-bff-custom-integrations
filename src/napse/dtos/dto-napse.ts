export class CreateOrderRequest {
  ref: string;
  order_id: number;
  account_id: number;
  account_access_token: string;
  master_access_token: string;
}
export class CreateOrderResponse {
  status: number;
  message: string;
}
export class ErrorResponse {
  error_code: string;
  error_message: string;
}
export class OrderStatusResponse {
  order_id: string;
  status: string;
}
