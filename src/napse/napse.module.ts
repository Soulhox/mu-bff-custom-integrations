import { Module } from '@nestjs/common';
import { NapseController } from './controller/napse-controller';
import { NapseService } from './core/services/napse.service';
import { CustomIntegrationProvider } from './infrastructure/providers/custom-integration-provider';
@Module({
  controllers: [NapseController],
  providers: [
    NapseService,
    {
      provide: 'CustomIntegrationApi',
      useClass: CustomIntegrationProvider,
    },
  ],
  imports: [],
})
export class NapseModule {}
