import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NapseController } from './napse/controller/napse-controller';
import { NapseModule } from './napse/napse.module';
import { OauthModule } from './oauth/oauth.module';

@Module({
  imports: [ConfigModule.forRoot(), OauthModule, NapseModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
