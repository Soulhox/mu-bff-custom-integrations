import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/check')
  public check() {
    return {
      service: 'bff-custom-integrations',
      status: 'ok',
    };
  }
}
